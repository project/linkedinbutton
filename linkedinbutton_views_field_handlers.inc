<?php

class views_handler_field_linkedinbutton extends views_handler_field {
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
  }


  function option_definition() {
    $options = parent::option_definition();
    $options['linkedinbutton_button'] = array('default' => 'top');
    return $options;
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }


  function options_form(&$form, &$form_state) {
    parent::options_form($form, &$form_state);

    $form['linkedinbutton_button'] = array(
      '#type' => 'select',
      '#options' => array(
        'top' => t('Vertical Count'),
        'right' => t('Horizontal Count'),
        'none'   => t('No count'),
      ),
      '#title' => t('LinkedIn Share Button type'),
      '#default_value' => $this->options['linkedinbutton_button'],
    );
  }

  function render($values) {
    $nid = $values->nid;
    $options['type'] = $this->options['linkedinbutton_button'];
    $output = theme('linkedinbutton_display', node_load($nid), $options);
    return $output;
  }
}
