<?php

function linkedinbutton_admin_settings() {
	$form = array();
  
  $form['button'] = array(
    '#type' => 'fieldset',
    '#title' => t('Choose your button. Customize it'),
  );
  
  $form['button']['linkedinbutton_button'] = array(
    '#type' => 'select',
    '#options' => array(
      'top' => t('Vertical Count'),
      'right' => t('Horizontal Count'),
      'none'   => t('No count'),
    ),
    '#default_value' => variable_get('linkedinbutton_button', 'top'),
    '#id' => 'linkedinbutton-button',
  );
  
  $form['node'] = array(
    '#type' => 'fieldset',
    '#title' => t('Select node settings.'),
  );

  $form['node']['linkedinbutton_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('linkedinbutton_node_types', array('story')),
  );

  $form['node']['linkedinbutton_node_location'] = array(
    '#type'    => 'checkboxes',
    '#title'   => t('Locations'),
    '#options' => array(
      'full'    => t('Full View'),
      'teaser'  => t('Teasers'),
      'links'   => t('Node links'),
    ),
    '#default_value' => variable_get('linkedinbutton_node_location', array('full')),
  );


  $form['node']['linkedinbutton_node_weight'] = array(
    '#type'          => 'weight',
    '#title'         => t('Weight'),
    '#default_value' => variable_get('linkedinbutton_node_weight', -5),
    '#description'   => t('Heavier weight will sink button to bottom on node view. This is also configurable per content type'),
  );

  
  return system_settings_form($form);
}
