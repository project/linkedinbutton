<?php

function linkedinbutton_views_data() {
  $data = array();
  
  $data['node']['linkedinbutton'] = array(
    'field' => array(
      'title' => t('LinkedIn button'),
      'help' => t('Provide a linkedin button field.'),
      'handler' => 'views_handler_field_linkedinbutton',
    ),
  );

  return $data;
}
